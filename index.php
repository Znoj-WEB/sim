<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Serialy</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

	  <link rel="stylesheet" href="material/material.min.css">
	  <link rel="stylesheet" href="styles.css">
	  <script defer src="material/material.min.js"></script>
  	  <!--getmdl-select-->   
	  <link rel="stylesheet" href="getmdl-select/getmdl-select.min.css">
	  <script defer src="getmdl-select/getmdl-select.min.js"></script>

	  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
	
	.demo-list-item {
	  width: 300px;
	}
    </style>
  </head>
  <body>
  
  <?php 
  function readlastline($fileName) { 
		$fileName = "db/".$fileName;
		if(file_exists($fileName)){
			$fp = @fopen($fileName, "r"); 
		}
		else{
			$fp = @fopen($fileName, "w+"); 
			file_put_contents($fileName, "\n");
		}
		$pos = -1; 
		$t = " "; 
		while ($t != "\n") { 
			 fseek($fp, $pos, SEEK_END); 
			 $t = fgetc($fp); 
			 $pos = $pos - 1; 
		} 
		$t = fgets($fp); 
		fclose($fp); 
		return $t;
  }
  
  $shows = [
		"Simpsons",
		"BigBang",
		"Friends",
		"GameOfThrones",
		"HowImet",
		"Futurama"
	];
  
  $lastSeriePost = $_POST["serie"];
  $showPost = $_POST["show"];

  if($lastSeriePost != null && $showPost != null && $_POST["dil"]){
	  if(intval($lastSeriePost) < 9){
		  $lastSeriePost = "0".$lastSeriePost;
	  }
	  $lastDilPost = $_POST["dil"];
	  if(intval($lastDilPost) < 9){
		  $lastDilPost = "0".$lastDilPost;
	  }
  
	$lastPost = "S".$lastSeriePost."E".$lastDilPost;
	file_put_contents('db/'.$showPost, "\n" . $lastPost, FILE_APPEND);
  }
  
  
  for($i = 0; $i < count($shows); $i++){
	  $last[$i] = readlastline($shows[$i]);
	  preg_match_all('/([\d]+)/', $last[$i], $matches[$i]);
	  $lastSerie = intval($matches[$i][0][0]);
  }

  ?>
	
  
    <div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
      <header class="demo-header mdl-layout__header mdl-layout__header--scroll mdl-color--grey-100 mdl-color-text--grey-800">
        <div class="demo-container mdl-grid">
		  <?php for($i = 0; $i < count($shows); $i++){ ?>
			<span class="mdl-cell mdl-cell--3-col"><?php echo $shows[$i]." - ".$last[$i]; ?></span>
		  <?php } ?>
		</div>
      </header>
      <div class="demo-ribbon"></div>
      <main class="demo-main mdl-layout__content">
		<div class="demo-container mdl-grid">
			<?php for($i = 0; $i < count($shows); $i++){ ?>
				<a name=<?php echo $shows[$i] ?>></a>
				<div class="mdl-cell demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell--4-col"> 
					<div class="mdl-layout__header-row">
						<?php if($i == 0){ ?>
							<span class="mdl-layout-title"><a href="http://simpsonovi.nikee.net/index.php"><?php echo $shows[$i]." - ".$last[$i]; ?></a></span>
						<?php } else { ?>
							<span class="mdl-layout-title"><?php echo $shows[$i]." - ".$last[$i]; ?></span>
						<?php } ?>
					</div>
				    <div class="row">
						<form class="col s12" action="index.php#<?php echo $shows[$i] ?>" method="post">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
							  <input class="mdl-textfield__input" id="serie<?php echo $i ?>" name="serie" value="<?php 
								  $last[$i] = readlastline($shows[$i]);
								  preg_match_all('/([\d]+)/', $last[$i], $matches[$i]);
								  echo intval($matches[$i][0][0]);
							  ?>" type="text" readonly tabIndex="-1" data-val=""/>
							  <label class="mdl-textfield__label" for="serie<?php echo $i ?>">Série</label>
							  <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu" id="serie<?php echo $i ?>" name="serie" for="serie<?php echo $i ?>">
								  <?php
									  for($x = 1; $x < 30; $x++){
										echo "<li class=\"mdl-menu__item\" value=\"$x\">  $x</li>";
									  } 
								  ?>
							</div>
						
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
								<input class="mdl-textfield__input" id="dil<?php echo $i ?>" name="dil" value="<?php 
								  $last[$i] = readlastline($shows[$i]);
								  preg_match_all('/([\d]+)/', $last[$i], $matches[$i]);
								  echo intval($matches[$i][0][1] + 1);
								?>" type="text" readonly tabIndex="-1" data-val=""/>
								<label class="mdl-textfield__label" for="dil<?php echo $i ?>">Díl</label>
								<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu" id="dil<?php echo $i ?>" name="dil" for="dil<?php echo $i ?>">
								  <?php
									  for($x = 0; $x < 30; $x++){
										echo "<li class=\"mdl-menu__item\" value=\"$x\">  $x</li>";
									  } 
								  ?>
							</div>
							<input id="show<?php echo $i ?>" name="show" type="hidden" value="<?php echo $shows[$i] ?>">
							<br />
							<input type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" value="Odeslat">
						</form>
					</div>
				</div>
			<?php } ?>
		</div>
        <footer class="demo-footer mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <ul class="mdl-mini-footer--link-list">
              <li><a href="https://znoj.cz">Jiří Znoj</a></li>
            </ul>
          </div>
        </footer>
      </main>
    </div>
    <a href="https://znoj.cz" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">Kontakt</a>
    
  </body>
</html>
