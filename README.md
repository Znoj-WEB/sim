### **Description**
Bookmarks for my favourite shows.

---
### **Link**
[https://bookmarks.znoj.cz/](https://bookmarks.znoj.cz/)

---
### **Technology**
HTML, PHP, Material Design Lite, GitLab CI/CD

---
### **Year**
2017

---
### **Screenshots**
Main:  
![](./README/main.png)  
  
Dropdown:  
![dropdown](README/dropdown.png)  
